/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package intents;

import resources.control.Intent;
import resources.objects.SWGObject;
import resources.player.Player;

/**
 *
 * @author Mads
 */
public class RequestBadgesIntent extends Intent {
	
	public static final String TYPE = "RequestBadgesIntent";
	
	private final Player requester;
	private final SWGObject target;
	
	public RequestBadgesIntent(Player requester, SWGObject target) {
		super(TYPE);
		this.requester = requester;
		this.target = target;
	}

	public Player getRequester() {
		return requester;
	}

	public SWGObject getTarget() {
		return target;
	}
	
}
