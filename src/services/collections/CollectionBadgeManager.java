/**
 * *********************************************************************************
 * Copyright (c) 2015 /// Project SWG /// www.projectswg.com                        *
 *                                                                                  *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on           *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies.  *
 * Our goal is to create an emulator which will provide a server for players to     *
 * continue playing a game similar to the one they used to play. We are basing      *
 * it on the final publish of the game prior to end-game events.                    *
 *                                                                                  *
 * This file is part of Holocore.                                                   *
 *                                                                                  *
 * -------------------------------------------------------------------------------- *
 *                                                                                  *
 * Holocore is free software: you can redistribute it and/or modify                 *
 * it under the terms of the GNU Affero General Public License as                   *
 * published by the Free Software Foundation, either version 3 of the               *
 * License, or (at your option) any later version.                                  *
 *                                                                                  *
 * Holocore is distributed in the hope that it will be useful,                      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
 * GNU Affero General Public License for more details.                              *
 *                                                                                  *
 * You should have received a copy of the GNU Affero General Public License         *
 * along with Holocore.  If not, see <http://www.gnu.org/licenses/>. * *
 * *********************************************************************************
 */
package services.collections;

import intents.GrantBadgeIntent;
import intents.RequestBadgesIntent;
import intents.chat.ChatBroadcastIntent;
import java.util.HashMap;
import java.util.Map;
import network.packets.swg.zone.BadgesResponseMessage;
import resources.client_info.ClientFactory;
import resources.client_info.visitors.DatatableData;
import resources.control.Intent;
import resources.control.Manager;
import resources.encodables.ProsePackage;
import resources.encodables.StringId;
import resources.objects.SWGObject;
import resources.objects.creature.CreatureObject;
import resources.objects.player.Badges;
import resources.objects.player.PlayerObject;
import resources.player.Player;
import resources.server_info.Log;

public class CollectionBadgeManager extends Manager {

	//TODO 
	//research categories
	//music
	//fix to appropriate message ex: kill_merek_activation_01
	private final ExplorationBadgeService explorationBadgeService;

	private DatatableData collectionTable;
	private Map<String, BadgeData> badgeMap;

	public CollectionBadgeManager() {
		collectionTable = (DatatableData) ClientFactory.getInfoFromFile("datatables/badge/badge_map.iff");
		badgeMap = new HashMap<>();
		explorationBadgeService = new ExplorationBadgeService();

		addChildService(explorationBadgeService);

		registerForIntent(GrantBadgeIntent.TYPE);
		registerForIntent(RequestBadgesIntent.TYPE);
	}

	@Override
	public void onIntentReceived(Intent i) {
		if (i instanceof GrantBadgeIntent) {
			handleBadgeGrant(((GrantBadgeIntent) i));
		} else if (i instanceof RequestBadgesIntent) {
			handleBadgesRequest((RequestBadgesIntent) i);
		}
	}

	@Override
	public boolean initialize() {
		for (int row = 0; row < collectionTable.getRowCount(); row++) {
			int badgeIndex = (int) collectionTable.getCell(row, 0);	// Index
			String badgeName = (String) collectionTable.getCell(row, 1);	// Badge name
			int category = (int) collectionTable.getCell(row, 3);	// Category
			String type = (String) collectionTable.getCell(row, 5);	// Type

			boolean explorationBadge = category == 2 && !type.equals("accumulation");

			badgeMap.put(badgeName, new BadgeData(badgeIndex, explorationBadge));
		}

		return super.initialize();
	}

	private void handleBadgeGrant(GrantBadgeIntent i) {
		CreatureObject target = i.getCreature();
		String badgeName = i.getCollectionBadgeName();

		if (badgeMap.containsKey(badgeName)) {

			Badges badges = target.getPlayerObject().getBadges();

			if (!badges.hasBadge(badgeMap.get(badgeName).getBadgeIndex())) {
				// They don't already have this badge
				giveBadge(target.getPlayerObject(), badgeName, true);
			}
		} else {
			Log.e(this, "%s could not receive badge %s because it does not exist", target, badgeName);
		}
	}

	private void handleBadgesRequest(RequestBadgesIntent i) {
		SWGObject target = i.getTarget();
		Player requester = i.getRequester();

		if (target != null) {
			if (target instanceof CreatureObject) {
				PlayerObject playerObject = ((CreatureObject) target).getPlayerObject();

				if (playerObject != null) {
					requester.sendPacket(new BadgesResponseMessage(target.getObjectId(), playerObject.getBadges()));
				} else {
					System.err.println(requester + " attempted to request badges of a NPC.");
				}
			} else {
				System.err.println(requester + " attempted to request badges of a non-creature target.");
			}
		} else {
			System.err.println(requester + " attempted to request badges of a null target.");
		}
	}

	private void giveBadge(PlayerObject target, String badgeName, boolean playMusic) {
		Badges badges = target.getBadges();

		BadgeData badgeData = badgeMap.get(badgeName);

		if (badgeData != null) {
			boolean explorationBadge = badgeData.isExplorationBadge();
			badges.set(badgeData.getBadgeIndex(), explorationBadge, true);

			new ChatBroadcastIntent(target.getOwner(), new ProsePackage(new StringId("badge_n", "prose_grant"), "TO", "@badge_n:" + badgeName)).broadcast();

			if (playMusic) {
				// TODO play music...
			}

			// Check exploration badge accumulation badges
			if (explorationBadge) {
				switch (badges.getExplorationBadgeCount()) {
					case 10:
						giveBadge(target, "bdg_exp_10_badges", false);
						break;
					case 20:
						giveBadge(target, "bdg_exp_20_badges", false);
						break;
					case 30:
						giveBadge(target, "bdg_exp_30_badges", false);
						break;
					case 40:
						giveBadge(target, "bdg_exp_40_badges", false);
						break;
					case 45:
						giveBadge(target, "bdg_exp_45_badges", false);
						break;
				}
			}

			// Check accumulation badges
			switch (badges.getBadgeCount()) {
				case 5:
					giveBadge(target, "count_5", false);
					break;
				case 10:
					giveBadge(target, "count_10", false);
					break;
				case 25:
					giveBadge(target, "count_25", false);
					break;
				case 50:
					giveBadge(target, "count_50", false);
					break;
				case 75:
					giveBadge(target, "count_75", false);
					break;
				case 100:
					giveBadge(target, "count_100", false);
					break;
				case 125:
					giveBadge(target, "count_125", false);
					break;
			}
		} else {
			System.err.println(target + " could not receive badge " + badgeName + " because it does not exist.");
		}

	}

	private class BadgeData {

		private final int badgeIndex;
		private final boolean explorationBadge;

		public BadgeData(int badgeIndex, boolean explorationBadge) {
			this.badgeIndex = badgeIndex;
			this.explorationBadge = explorationBadge;
		}

		public int getBadgeIndex() {
			return badgeIndex;
		}

		public boolean isExplorationBadge() {
			return explorationBadge;
		}
	}

}
