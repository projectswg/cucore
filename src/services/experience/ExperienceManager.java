/***********************************************************************************
* Copyright (c) 2015 /// Project SWG /// www.projectswg.com                        *
*                                                                                  *
* ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on           *
* July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies.  *
* Our goal is to create an emulator which will provide a server for players to     *
* continue playing a game similar to the one they used to play. We are basing      *
* it on the final publish of the game prior to end-game events.                    *
*                                                                                  *
* This file is part of Holocore.                                                   *
*                                                                                  *
* -------------------------------------------------------------------------------- *
*                                                                                  *
* Holocore is free software: you can redistribute it and/or modify                 *
* it under the terms of the GNU Affero General Public License as                   *
* published by the Free Software Foundation, either version 3 of the               *
* License, or (at your option) any later version.                                  *
*                                                                                  *
* Holocore is distributed in the hope that it will be useful,                      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
* GNU Affero General Public License for more details.                              *
*                                                                                  *
* You should have received a copy of the GNU Affero General Public License         *
* along with Holocore.  If not, see <http://www.gnu.org/licenses/>.                *
*                                                                                  *
***********************************************************************************/
package services.experience;

import intents.chat.ChatBroadcastIntent;
import intents.experience.ExperienceIntent;
import intents.experience.LevelChangedIntent;
import java.awt.Color;
import intents.experience.GrantSkillIntent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import network.packets.swg.zone.object_controller.ShowFlyText;
import network.packets.swg.zone.object_controller.ShowFlyText.Scale;
import resources.common.RGB;
import resources.config.ConfigFile;
import resources.control.Intent;
import resources.control.Manager;
import resources.encodables.OutOfBandPackage;
import resources.encodables.ProsePackage;
import resources.encodables.StringId;
import resources.objects.creature.CreatureObject;
import resources.objects.player.PlayerObject;
import resources.server_info.Log;
import resources.server_info.RelationalDatabase;
import resources.server_info.RelationalServerFactory;

/**
 * The {@code ExperienceManager} listens for {@link ExperienceIntent} and
 * grants XP based on it.
 * @author Mads
 */
public final class ExperienceManager extends Manager {
	
	private static final String GET_ALL_LEVELS = "SELECT * FROM player_level";
	private static final String GET_ALL_MULTIPLIERS = "SELECT * FROM combat_xp_multipliers";
	private static final String COMBAT_XP_TYPE = "combat_general";
	
	private SkillService skillService;
	private final Map<Short, Integer> levelXpMap;
	private final Map<String, Integer> combatXpMultiplierMap;	// Maps XP type to a multiplier for Combat XP
	private final double xpMultiplier;
	
	public ExperienceManager() {
		skillService = new SkillService();
		levelXpMap = new HashMap<>();
		combatXpMultiplierMap = new HashMap<>();
		xpMultiplier = getConfig(ConfigFile.FEATURES).getDouble("XP-MULTIPLIER", 1);
		
		registerForIntent(ExperienceIntent.TYPE);
		registerForIntent(GrantSkillIntent.TYPE);
		
		addChildService(skillService);
	}

	@Override
	public boolean initialize() {
		try (RelationalDatabase spawnerDatabase = RelationalServerFactory.getServerData("experience/player_level.db", "player_level")) {
			try (ResultSet set = spawnerDatabase.executeQuery(GET_ALL_LEVELS)) {
				while (set.next()) {
					// Load player level
					levelXpMap.put(set.getShort("level"), set.getInt("required_combat_xp"));
					// TODO store level granted health
				}
			}
		} catch (SQLException e) {
			Log.e(this, e);
		}
		
		try (RelationalDatabase spawnerDatabase = RelationalServerFactory.getServerData("experience/combat_xp_multipliers.db", "combat_xp_multipliers")) {
			try (ResultSet set = spawnerDatabase.executeQuery(GET_ALL_MULTIPLIERS)) {
				while (set.next()) {
					combatXpMultiplierMap.put(set.getString("xp_type"), set.getInt("multiplier"));
				}
			}
		} catch (SQLException e) {
			Log.e(this, e);
		}
		
		return super.initialize();
	}
	
	@Override
	public void onIntentReceived(Intent i) {
		switch(i.getType()) {
			case ExperienceIntent.TYPE:
				if (i instanceof ExperienceIntent)
					handleExperienceGainedIntent((ExperienceIntent) i);
				break;
			case GrantSkillIntent.TYPE:
				if (i instanceof GrantSkillIntent)
					handleGrantSkillIntent((GrantSkillIntent) i);
				break;
		}
	}
	
	private void handleExperienceGainedIntent(ExperienceIntent i) {
		CreatureObject creatureObject = i.getCreatureObject();
		PlayerObject playerObject = creatureObject.getPlayerObject();
		if (playerObject != null) {
			String xpType = i.getXpType();
			int experienceGained = i.getExperienceGained();
			// Give the player their increased XP
			awardExperience(creatureObject, playerObject, xpType, experienceGained);
			// Calculate the amount of Combat XP to also grant the player
			
			if (combatXpMultiplierMap.containsKey(xpType)) {
				// Give Combat XP, which works toward their combat level
				awardExperience(creatureObject, playerObject, COMBAT_XP_TYPE, experienceGained * combatXpMultiplierMap.get(xpType));
			}
		}
	}
	
	private void handleGrantSkillIntent(GrantSkillIntent i) {
		CreatureObject creatureObject = i.getTarget();

		// Try to change their combat level
		short oldLevel = creatureObject.getLevel();
		attemptLevelUp(oldLevel, creatureObject, creatureObject.getPlayerObject().getExperiencePoints(COMBAT_XP_TYPE));
		short newLevel = creatureObject.getLevel();

		if (oldLevel < newLevel) {	// If we've leveled up at least once
			new LevelChangedIntent(creatureObject, oldLevel, newLevel).broadcast();
//					adjustHealth(creatureObject, newLevel);
//					adjustAction(creatureObject, newLevel);
			Log.i(this, "%s leveled from %d to %d", creatureObject, oldLevel, newLevel);
		}
	}
	
	private int awardExperience(CreatureObject creatureObject, PlayerObject playerObject, String xpType, int xpGained) {
		Integer currentXp = playerObject.getExperiencePoints(xpType);
		int newXpTotal;
		
		xpGained *= xpMultiplier;
		
		if (currentXp == null) {	// They don't have this type of XP already
			newXpTotal = xpGained;
		} else {	// They already have this kind of XP - add gained to current
			newXpTotal = currentXp + xpGained;
		}
		
		playerObject.setExperiencePoints(xpType, newXpTotal);
		Log.d(this, "%s gained %d %s XP", creatureObject, xpGained, xpType);
		
		// Show flytext above the creature that received XP, but only to them
		creatureObject.sendSelf(new ShowFlyText(creatureObject.getObjectId(), new OutOfBandPackage(new ProsePackage(new StringId("base_player", "prose_flytext_xp"), "DI", xpGained)), Scale.MEDIUM, new RGB(Color.magenta)));
		
		// TODO CU: flytext is displayed over the killed creature
		// TODO CU: is the displayed number the gained Combat XP with all bonuses applied?
		
		// TODO only display in console. Isn't displayed for Combat XP.
		// TODO display different messages with inspiration bonus and/or group bonus
		new ChatBroadcastIntent(creatureObject.getOwner(), new ProsePackage(new StringId("base_player", "prose_grant_xp"), "TO", new StringId("exp_n", xpType))).broadcast();
		return newXpTotal;
	}
	
	private short attemptLevelUp(short currentLevel, CreatureObject creatureObject, int newXpTotal) {
		if (currentLevel >= getMaxLevel()) {
			return currentLevel;
		}
		
		short nextLevel = (short) (currentLevel + 1);
		Integer xpNextLevel = levelXpMap.get(nextLevel);

		if (xpNextLevel == null) {
			Log.e(this, "%s couldn't level up to %d because there's no XP requirement", creatureObject, nextLevel);
			return currentLevel;
		}

		// Recursively attempt to level up again, in case we've gained enough XP to level up multiple times.
		return newXpTotal >= xpNextLevel ? attemptLevelUp(nextLevel, creatureObject, newXpTotal) : currentLevel;
	}
	
	private void adjustHealth(CreatureObject creatureObject, short newLevel) {
		int currentLevelHealthGranted = creatureObject.getLevelHealthGranted();	// The existing levelHealthGranted
		int newLevelHealthGranted = 100 * newLevel;	// new levelHealthGranted
		int difference = newLevelHealthGranted - currentLevelHealthGranted;
		
		// Set new levelHealthGranted
		creatureObject.setLevelHealthGranted(newLevelHealthGranted);
		
		// Add the difference to their max health
		int newMaxHealth = creatureObject.getMaxHealth() + difference;
		creatureObject.setMaxHealth(newMaxHealth);
		
		// Give them full health
		creatureObject.setBaseHealth(newMaxHealth);
		creatureObject.setHealth(newMaxHealth);
	}
	
	private void adjustAction(CreatureObject creatureObject, short newLevel) {
		int currentMaxAction = creatureObject.getMaxAction();
		int newLevelActionGranted = 75 * newLevel;
		int difference = newLevelActionGranted - currentMaxAction;
		
		// Add the difference to their max action
		int newMaxAction = currentMaxAction + difference;
		creatureObject.setMaxAction(newMaxAction);
		
		// Give them full action
		creatureObject.setBaseAction(newMaxAction);
		creatureObject.setAction(newMaxAction);
	}
	
	private int getMaxLevel() {
		return levelXpMap.size();
	}
}
